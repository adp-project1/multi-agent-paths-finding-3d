#include <iostream>
#include <fstream>

#include <yaml-cpp/yaml.h>

using namespace std;

#include <stdlib.h>
#include <math.h>
#include <tuple>
#include <set>

using namespace std;

struct Coordinate {
    Coordinate() = default;

   Coordinate( int x, int y, int z) :  x(x), y(y), z(z) {}

  double x;
  double y;
  double z;
};

int main()
{
    //std::vector<std::vector<Coordinate> > Klinikum;
    //std::vector<std::vector<Coordinate> > Alice;
    //std::vector<std::vector<Coordinate> > Elisaheth;
    //std::vector<std::vector<Coordinate> > Polizei;
    //std::vector<std::vector<Coordinate> > Federal;
    //std::vector<std::vector<Coordinate> > Flughafen;
    std::ofstream out("../obstacle.yaml");
    out << "obstacles:" << std::endl;
    Coordinate Klinikum;
    for ( unsigned long i = 367 ; i < 388; i++) {
        for (unsigned long j = 173; j < 185; j++) {
            for (unsigned long k = 0; k < 11; k++) {
                Klinikum.x = i;
                Klinikum.y = j;
                Klinikum.z = k;
                out << " - [" << Klinikum.x << "," << Klinikum.y << "," << Klinikum.z << "]" << endl;
            }
        }
    }

    Coordinate Alice;
    for ( unsigned long i = 432 ; i < 457; i++) {
        for (unsigned long j = 204; j < 185; j++) {
            for (unsigned long k = 0; k < 11; k++) {
                Alice.x = i;
                Alice.y = j;
                Alice.z = k;
                out << " - [" << Alice.x << "," << Alice.y << "," << Alice.z << "]" << endl;
            }
        }
    }

    Coordinate Elisabeth;
    for ( unsigned long i = 435 ; i < 453; i++) {
        for (unsigned long j = 170; j < 184; j++) {
            for (unsigned long k = 0; k < 11; k++) {
                Elisabeth.x = i;
                Elisabeth.y = j;
                Elisabeth.z = k;
                out << " - [" << Elisabeth.x << "," << Elisabeth.y << "," << Elisabeth.z << "]" << endl;
            }
        }
    }

    Coordinate Polizei;
    for ( unsigned long i = 385 ; i < 395; i++) {
        for (unsigned long j = 182; j < 192; j++) {
            for (unsigned long k = 0; k < 11; k++) {
                Polizei.x = i;
                Polizei.y = j;
                Polizei.z = k;
                out << " - [" << Polizei.x << "," << Polizei.y << "," << Polizei.z << "]" << endl;
            }
        }
    }

    Coordinate Federal;
    for ( unsigned long i = 387 ; i < 403; i++) {
        for (unsigned long j = 165; j < 178; j++) {
            for (unsigned long k = 0; k < 11; k++) {
                Federal.x = i;
                Federal.y = j;
                Federal.z = k;
                out << " - [" << Federal.x << "," << Federal.y << "," << Federal.z << "]" << endl;
            }
        }
    }

    Coordinate Flughafen;
    for ( unsigned long i = 94 ; i < 240; i++) {
        for (unsigned long j = 21; j < 110; j++) {
            for (unsigned long k = 0; k < 11; k++) {
                Flughafen.x = i;
                Flughafen.y = j;
                Flughafen.z = k;
                out << " - [" << Flughafen.x << "," << Flughafen.y << "," << Flughafen.z << "]" << endl;
            }
        }
    }
    //cout << "Hello World!" << endl;
    return 0;
}

#include <iostream>
#include <fstream>

#include <yaml-cpp/yaml.h>

using namespace std;

#include <stdlib.h>
#include <math.h>
#include <tuple>
#include <set>
#include <boost/functional/hash.hpp>
#include <boost/program_options.hpp>


struct Coordinate {
    Coordinate() = default;

   Coordinate( int x, int y, int z) :  x(x), y(y), z(z) {}

  int x;
  int y;
  int z;
};
struct Speed {
    Speed() = default;
    Speed(int vx, int vy, int vz) : vx(vx), vy(vy), vz(vz) {}

    int vx;
    int vy;
    int vz;

};



struct Agents {
    //Agents(std::vector<int> radii, std::vector<int> speeds, std::vector<int> startTimeSteps, std::vector<Coordinate> start, std::vector<Coordinate> goal)
     //   :  radii(radii), speeds(speeds), startTimeSteps(startTimeSteps), start(start), goal(goal){}

    std::vector<float> radii;
    std::vector<Speed> speeds;
    std::vector<int> startTimeSteps;
    std::vector<Coordinate> starts;
    std::vector<Coordinate> goals;
};

int main(int argc, char* argv[])
{

    cout << "Welcome to XXXX Project - Input Gneration" << endl;
    cout << "The Input Data will be generated as a YAML File" << endl;
    namespace po = boost::program_options;
    std::cout << "    " << std::endl;
    std::cout << "-------------------------------------" << endl;
    po::options_description desc("Allowed options");
    std::string inputFile;
    std::string outputFile;
    desc.add_options()("help", "produce help message")(
                "input,i", po::value<std::string>(&inputFile)->required(),
                "input file (YAML)")("output,o",
                                     po::value<std::string>(&outputFile)->required(),
                                     "output file (YAML)");
    try {
      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, desc), vm);
      po::notify(vm);

      if (vm.count("help") != 0u) {
        std::cout << desc << "\n";
        return 0;
      }
    } catch (po::error& e) {
      std::cerr << e.what() << std::endl << std::endl;
      std::cerr << desc << std::endl;
      return 1;
    }
    std::cout << "-------------------------------------" << endl;
    std::cout << "    " << std::endl;
    cout << "Please enter the SERIAL Number of the First UAV and TOTAL Amount of UAVs in this batch" << endl;
    int num = 0;
    int TotalAmount = 0;
    std::cin >> num >> TotalAmount;
    assert(TotalAmount != 0);
    Agents agents;
    std::cout << "    " << std::endl;

    for (int i = num; i < num + TotalAmount; ++i){
        std::cout << "    " << std::endl;
        std::cout << "Agent:" << i << endl;
        Speed speed;
        std::cout << "    " << std::endl;
        std::cout << "------Please enter the speed(vx, vy, vz)(voxel/s) of the UAV------" << endl;
        std::cin >> speed.vx >> speed.vy >> speed.vz;
        agents.speeds.push_back(speed);
        std::cout << "the speed of agent [" << i << "] is （" << speed.vx << ", "<< speed.vy << ", " << speed.vz << " ）" << endl;
        std::cout << "    " << std::endl;

        float radius = 0;
        std::cout << "    " << std::endl;
        std::cout << "------Please give the radius(m) of the UAV-----" << endl;
        std::cin >> radius;
        agents.radii.push_back(radius);
        std::cout << "the radius of agent [" << i << "] is " << radius << endl;
        std::cout << "    " << std::endl;

        int startTime = 0;
        std::cout << "    " << std::endl;
        std::cout << "------Please give the startTime(s) of the UAV---" << endl;
        std::cin >> startTime;
        agents.startTimeSteps.push_back(startTime);
        std::cout << "the startTime of agent [" << i << "] is " << startTime << endl;//  use Timer, please check the timer type
        std::cout << "    " << std::endl;

        Coordinate start;
        start.x = 0; start.y = 0; start.z = 0;
        std::cout << "    " << std::endl;
        std::cout << "------Please give the start(x, y, z) of the UAV--" << endl;
        std::cin >> start.x >> start.y >> start.z;
        agents.starts.push_back(start);
        std::cout << "the start of agent [" << i << "] is( " << start.x << "," << start.y<< "," << start.z << ")" << endl;
        std::cout << "    " << std::endl;

        Coordinate goal;
        goal.x = 0; goal.y = 0; goal.z = 0;
        std::cout << "    " << std::endl;
        std::cout << "------Please give the goal(x, y, z) of the UAV----------" << endl;
        std::cin >> goal.x >> goal.y >> goal.z;
        agents.goals.push_back(goal);
        std::cout << "the goal of Agent [" << i << "] is( " << goal.x << "," << goal.y<< "," << goal.z << ")" << endl;
        std::cout << "----------------------------------------------" << endl;
        std::cout << "    " << std::endl;

    }

    std::cout << "All the UAV data have been entered" << endl;
    std::cout << "Now ENTER the map dimensions " << endl;

    Coordinate dimension;
    dimension.x = 0; dimension.y = 0; dimension.z = 0;
    std::cin >> dimension.x >> dimension.y >> dimension.z;
    std::cout << "the dimension of scenario is(" << dimension.x << "," << dimension.y<< "," << dimension.z << ")" << endl;
    std::cout << "----------------------------------------------" << endl;
    std::cout << "Please make sure the file 'obstacle.yaml' has been left in the project folder! " << endl;
    std::cout << "----------------------------------------------" << endl;
    std::vector<Coordinate> obstacles;
    YAML::Node config = YAML::LoadFile(inputFile); // please change the dictionary of  the obstacle source file
    for(const auto& node : config["obstacles"]) {
        obstacles.push_back(Coordinate(node[0].as<int>(), node[1].as<int>(), node[2].as<int>()));
    }


    std::cout << "The corresponded YAML file will be generated soon" << endl;

    std::cout << "-------------------------------------" << endl;

    //YAML::Node config = YAML::LoadFile("/home/quanqi/build-inputGeneration-Desktop-Default/input.yaml");

    std::ofstream out(outputFile);
        out << "agents:" << std::endl;
    for ( int i = num; i < num + TotalAmount; ++i) {
        out << "-  name: " << "agent" << i << std::endl;
        out << "   start: [" << agents.starts[i-num].x << ", " << agents.starts[i-num].y << ", " << agents.starts[i-num].z << "]" << endl
            << "   goal: [" << agents.goals[i-num].x << ", " << agents.goals[i-num].y << ", " << agents.goals[i-num].z << "]" << endl
            << "   radius: " << agents.radii[i-num] << endl
            << "   speed: [" << agents.speeds[i-num].vx << ", "<< agents.speeds[i-num].vy << ", "<< agents.speeds[i-num].vz << "]" << endl
            << "   startTimeStep: " << agents.startTimeSteps[i-num] << endl;
    }
    out << "map:" << endl;
    out << "    dimensions: [" << dimension.x << ", " << dimension.y << ", " << dimension.z << "]" << endl;
    out << "    obstacles: " << endl;
    for (unsigned long i = 0; i < obstacles.size(); i++) {
    out << "    - [" << obstacles[i].x << ", " << obstacles[i].y << ", " << obstacles[i].z << "]" << endl;
    }



    return 0;
}

#include <fstream>
#include <iostream>
#include <cmath>

#include <boost/functional/hash.hpp>
#include <boost/program_options.hpp>

#include <yaml-cpp/yaml.h>

#include "include/ecbs.hpp"
#include "timer.hpp"

#include <stdlib.h>
#include <math.h>

using libMultiUAVPlanning::ECBS;
using libMultiUAVPlanning::Neighbor;
using libMultiUAVPlanning::PlanResult;

struct State {
  State() = default;
  State(int time, int x, int y, int z) : time(time), x(x), y(y), z(z) {}

  bool operator==(const State& s) const {
    return time == s.time && x == s.x && y == s.y && z == s.z;
  }

  bool equalExceptTime(const State& s) const { return x == s.x && y == s.y && z == s.z; }

  friend std::ostream& operator<<(std::ostream& os, const State& s) {
    return os << s.time << ": (" << s.x << "," << s.y << "," << s.z << ")";
    // return os << "(" << s.x << "," << s.y << "," << s.z << ")";
  }

  int time;
  int x;
  int y;
  int z;
};

namespace std {
template <>
struct hash<State> {
  size_t operator()(const State& s) const {
    size_t seed = 0;
    boost::hash_combine(seed, s.time);
    boost::hash_combine(seed, s.x);
    boost::hash_combine(seed, s.y);
    boost::hash_combine(seed, s.z);
    return seed;
  }
};
}  // namespace std

///
enum class Action { // ENU Coordinate System
  Up,               // z+1
  Down,             // z-1
  Left,             // x+1
  Right,            // x-1
  Wait,             // stay still
    Accelerate,     // y+1
    Deaccelerate,   // y-1
};

std::ostream& operator<<(std::ostream& os, const Action& a) {
  switch (a) {
    case Action::Up:
      os << "Up";
      break;
    case Action::Down:
      os << "Down";
      break;
    case Action::Left:
      os << "Left";
      break;
    case Action::Right:
      os << "Right";
      break;
    case Action::Wait:
      os << "Wait";
      break;
    case Action::Accelerate:
      os << "Accelerate";
      break;
    case Action::Deaccelerate:
      os << "Deaccelerate";
      break;
  }
  return os;
}


struct Conflict {
  enum Type {
    Vertex,
    Edge,
  };

  int time;
  size_t agent1;
  size_t agent2;
  Type type;

  int x1;
  int y1;
  int z1;
  int x2;
  int y2;
  int z2;

  friend std::ostream& operator<<(std::ostream& os, const Conflict& c) {
    switch (c.type) {
      case Vertex:
        return os << c.time << ": Vertex(" << c.x1 << "," << c.y1 << "," << c.z1 << ")";
      case Edge:
        return os << c.time << ": Edge(" << c.x1 << "," << c.y1 << "," << c.z1 << "," << c.x2
                  << "," << c.y2 << "," << c.z2 << ")";
    }
    return os;
  }
}; // cancel

struct Conflict1 {
    int time;
    std::pair<int, int> conflictInterval; // Ic
    size_t agent1; // ai
    size_t agent2; // aj

    int x1;
    int y1;
    int z1; // p(i) at tc
    int radius1;
    float speed1;
    int x2;
    int y2;
    int z2; // p(j) at tc
    int radius2;
    float speed2;

    friend std::ostream& operator<<(std::ostream& os, const Conflict1& c) {
      return os << "from" << c.conflictInterval.first << "to" << c.conflictInterval.second << ": Conflict(" << c.x1 << "," << c.y1 << "," << c.z1 << "," << c.x2
                << "," << c.y2 << "," << c.z2 << ")";
    }

}; // Redefinition of conflict, will be used in getFirstConflict, added by quanqi

struct VertexConstraint {
  VertexConstraint(int time, int x, int y, int z) : time(time), x(x), y(y), z(z) {}
  int time;
  int x;
  int y;
  int z;

  bool operator<(const VertexConstraint& other) const {
    return std::tie(time, x, y, z) < std::tie(other.time, other.x, other.y, other.z);
  }

  bool operator==(const VertexConstraint& other) const {
    return std::tie(time, x, y, z) == std::tie(other.time, other.x, other.y, other.z);
  }

  friend std::ostream& operator<<(std::ostream& os, const VertexConstraint& c) {
    return os << "VC(" << c.time << "," << c.x << "," << c.y << ", " << c.z << ")";
  }
}; // cancel

namespace std {
template <>
struct hash<VertexConstraint> {
  size_t operator()(const VertexConstraint& s) const {
    size_t seed = 0;
    boost::hash_combine(seed, s.time);
    boost::hash_combine(seed, s.x);
    boost::hash_combine(seed, s.y);
    boost::hash_combine(seed, s.z);
    return seed;
  }
};
}  // namespace std

struct EdgeConstraint {
  EdgeConstraint(int time, int x1, int y1, int z1, int x2, int y2, int z2)
      : time(time), x1(x1), y1(y1), z1(z1), x2(x2), y2(y2), z2(z2) {}
  int time;
  int x1;
  int y1;
  int z1;
  int x2;
  int y2;
  int z2;

  bool operator<(const EdgeConstraint& other) const {
    return std::tie(time, x1, y1, z1, x2, y2, z2) <
           std::tie(other.time, other.x1, other.y1, other.z1, other.x2, other.y2, other.z2);
  }

  bool operator==(const EdgeConstraint& other) const {
    return std::tie(time, x1, y1, z1, x2, y2, z2) ==
           std::tie(other.time, other.x1, other.y1, other.z1, other.x2, other.y2, other.z2);
  }

  friend std::ostream& operator<<(std::ostream& os, const EdgeConstraint& c) {
    return os << "EC(" << c.time << "," << c.x1 << "," << c.y1 << "," << c.z1 << "," << c.x2
              << "," << c.y2 << ", " << c.z2 << ")";
  }
}; // cancel

namespace std {
template <>
struct hash<EdgeConstraint> {
  size_t operator()(const EdgeConstraint& s) const {
    size_t seed = 0;
    boost::hash_combine(seed, s.time);
    boost::hash_combine(seed, s.x1);
    boost::hash_combine(seed, s.y1);
    boost::hash_combine(seed, s.z1);
    boost::hash_combine(seed, s.x2);
    boost::hash_combine(seed, s.y2);
    boost::hash_combine(seed, s.z2);
    return seed;
  }
};
}  // namespace std

struct Constraints {
  std::unordered_set<VertexConstraint> vertexConstraints;
  std::unordered_set<EdgeConstraint> edgeConstraints;

  void add(const Constraints& other) {
    vertexConstraints.insert(other.vertexConstraints.begin(),
                             other.vertexConstraints.end());
    edgeConstraints.insert(other.edgeConstraints.begin(),
                           other.edgeConstraints.end());
  }

  bool overlap(const Constraints& other) const {
    for (const auto& vc : vertexConstraints) {
      if (other.vertexConstraints.count(vc) > 0) {
        return true;
      }
    }
    for (const auto& ec : edgeConstraints) {
      if (other.edgeConstraints.count(ec) > 0) {
        return true;
      }
    }
    return false;
  }

  friend std::ostream& operator<<(std::ostream& os, const Constraints& c) {
    for (const auto& vc : c.vertexConstraints) {
      os << vc << std::endl;
    }
    for (const auto& ec : c.edgeConstraints) {
      os << ec << std::endl;
    }
    return os;
  }
};// cancel


struct Constraint1 {
    Constraint1() = default;

    Constraint1(int x1, int y1, int z1,
                int lowTime1) :
                x1(x1), y1(y1), z1(z1),lowTime1(lowTime1) {}

    Constraint1(int x2, int y2, int z2,
                int lowTime1, int lowTime2) :
                x2(x2), y2(y2), z2(z2), lowTime1(lowTime1), lowTime2(lowTime2) {}

    Constraint1(int x1, int y1, int z1, int radius1, float speed1,
    int x2, int y2, int z2, int radius2, float speed2,
    std::pair<int, int> conflictInterval) :
    x1(x1), y1(y1), z1(z1), radius1(radius1), speed1(speed1), x2(x2), y2(y2), z2(z2), radius2(radius2), speed2(speed2),  conflictInterval(conflictInterval){}

    int x1;
    int y1;
    int z1; // p(i)
    int radius1;
    float speed1;
    int x2;
    int y2;
    int z2; // p(j)
    int radius2;
    float speed2;
    // Ic
    std::pair<int, int> conflictInterval;
    int lowTime1 = conflictInterval.first;
    int lowTime2 = conflictInterval.first;

    bool operator<(const Constraint1& other) const {
      return std::tie(conflictInterval, x1, y1, z1, radius1, speed1, x2, y2, z2, radius2, speed2) <
             std::tie(other.conflictInterval,
                      other.x1, other.y1, other.z1, other.radius1, other.speed1,
                      other.x2, other.y2, other.z2, other.radius2, other.speed2);
    }

    bool operator==(const Constraint1& other) const {
        return std::tie(conflictInterval, x1, y1, z1, radius1, speed1, x2, y2, z2, radius2, speed2) ==
               std::tie(other.conflictInterval,
                        other.x1, other.y1, other.z1, other.radius1, other.speed1,
                        other.x2, other.y2, other.z2, other.radius2, other.speed2);
    }

    friend std::ostream& operator<<(std::ostream& os, const Constraint1& c) {
      return os << "C(" << "from" << c.conflictInterval.first << "to" << c.conflictInterval.second << "," << c.x1 << "," << c.y1 << "," << c.z1 << "," << c.radius1 << "," << c.speed1 << ","
                << c.x2 << "," << c.y2 << ", " << c.z2 << "," << c.radius2 << "," << c.speed2 << ")";
    }
}; // Redefinition of constraint, will be used in createConstraintfromconflict function

namespace std {
template <>
struct hash<Constraint1> {
  size_t operator()(const Constraint1& s) const {
    size_t seed = 0;
    boost::hash_combine(seed, s.conflictInterval);
    boost::hash_combine(seed, s.x1);
    boost::hash_combine(seed, s.y1);
    boost::hash_combine(seed, s.z1);
    boost::hash_combine(seed, s.x2);
    boost::hash_combine(seed, s.y2);
    boost::hash_combine(seed, s.z2);
    boost::hash_combine(seed, s.radius1);
    boost::hash_combine(seed, s.radius2);
    boost::hash_combine(seed, s.speed1);
    boost::hash_combine(seed, s.speed2);
    return seed;
  }
};
}  // namespace std

struct Constraints1 {
  std::unordered_set<Constraint1> allConstraints;


  void add(const Constraints1& other) {
    allConstraints.insert(other.allConstraints.begin(),
                             other.allConstraints.end());
  }

  bool overlap(const Constraints1& other) const {
    for (const auto& c : allConstraints) {
      if (other.allConstraints.count(c) > 0) {
        return true;
      }
    }
    return false;
  }

  friend std::ostream& operator<<(std::ostream& os, const Constraints1& cs) {
    for (const auto& c : cs.allConstraints) {
      os << c << std::endl;
    }

    return os;
  }
};

struct Location {
  Location(int x, int y, int z) : x(x), y(y), z(z) {}
  int x;
  int y;
  int z;

  bool operator<(const Location& other) const {
    return std::tie(x, y, z) < std::tie(other.x, other.y, other.z);
  }

  bool operator==(const Location& other) const {
    return std::tie(x, y, z) == std::tie(other.x, other.y, other.z);
  }

  friend std::ostream& operator<<(std::ostream& os, const Location& c) {
    return os << "(" << c.x << "," << c.y << "," << c.z << ")";
  }
};

namespace std {
template <>
struct hash<Location> {
  size_t operator()(const Location& s) const {
    size_t seed = 0;
    boost::hash_combine(seed, s.x);
    boost::hash_combine(seed, s.y);
    boost::hash_combine(seed, s.z);
    return seed;
  }
};
}  // namespace std

///
class Environment {
 public:
  Environment(size_t dimx, size_t dimy, size_t dimz, std::unordered_set<Location> obstacles,
              std::vector<Location> goals)
      : m_dimx(dimx),
        m_dimy(dimy),
        m_dimz(dimz),
        m_obstacles(std::move(obstacles)),
        m_goals(std::move(goals)),
        m_agentIdx(0),
        m_constraints(nullptr),
        m_lastGoalConstraint(-1),
        m_highLevelExpanded(0),
        m_lowLevelExpanded(0) {}

  Environment(const Environment&) = delete;
  Environment& operator=(const Environment&) = delete;

  void setLowLevelContext(size_t agentIdx, const Constraints1* constraints) {
    assert(constraints);
    m_agentIdx = agentIdx;
    m_constraints = constraints;
    m_lastGoalConstraint = -1;
    for (const auto& c : constraints->allConstraints) {
        // find the coordinates of the last constraint
      if (c.x1 == m_goals[m_agentIdx].x && c.y1 == m_goals[m_agentIdx].y && c.z1 == m_goals[m_agentIdx].z) {
        m_lastGoalConstraint = std::max(m_lastGoalConstraint, c.conflictInterval.first); // record the timestep of the constraint
      }
      else if (c.x2 == m_goals[m_agentIdx].x && c.y2 == m_goals[m_agentIdx].y && c.z2 == m_goals[m_agentIdx].z) {
          m_lastGoalConstraint = std::max(m_lastGoalConstraint, c.conflictInterval.first);
      }
    }
  } // to find the end of deconfliction, modified by quanqi

   /* for (const auto& vc : constraints->vertexConstraints) {
      if (vc.x == m_goals[m_agentIdx].x && vc.y == m_goals[m_agentIdx].y && vc.z == m_goals[m_agentIdx].z) {
        m_lastGoalConstraint = std::max(m_lastGoalConstraint, vc.time);
      }
    }
  }*/

  int admissibleHeuristic(const State& s) {
    return std::abs(s.x - m_goals[m_agentIdx].x) +
           std::abs(s.y - m_goals[m_agentIdx].y) +
            std::abs(s.z - m_goals[m_agentIdx].z)
            ;
  }

  // low-level, estimates the number of conflicts
  int focalStateHeuristic(
      const State& s, int /*gScore*/,
      const std::vector<PlanResult<State, Action, int> >& solution) {
    int numConflicts = 0;
    // with neighbors, agent i at time of neighbor ,wether conflict or not, static
    for (size_t i = 0; i < solution.size(); ++i) {
      if (i != m_agentIdx && !solution[i].states.empty()) {
        State state2 = getState(i, solution, s.time);
        if (s.equalExceptTime(state2)) {
          ++numConflicts;
        }
      }
    }
    return numConflicts;
  } // keep it

  // low-level,estimates the number of conflicts
  int focalTransitionHeuristic(
      const State& s1a, const State& s1b, int /*gScoreS1a*/, int /*gScoreS1b*/,
      const std::vector<PlanResult<State, Action, int> >& solution) {
    int numConflicts = 0;
    // with neighbors, agent i moves from 1 to 2, wether conflict with neighbor or not
    for (size_t i = 0; i < solution.size(); ++i) {
      if (i != m_agentIdx && !solution[i].states.empty()) {
        State s2a = getState(i, solution, s1a.time);
        State s2b = getState(i, solution, s1b.time);
        if (s1a.equalExceptTime(s2b) && s1b.equalExceptTime(s2a)) {
          ++numConflicts;
        }
      }
    }
    return numConflicts;
  } // keep it

  // Count all conflicts, appear at a whole route
  int focalHeuristic(
      const std::vector<PlanResult<State, Action, int> >& solution,
          const std::vector<int>& radii, const std::vector<float>& speeds, const std::vector<int>& startTimes) {
    int numConflicts = 0;

    for (unsigned int i = 0; i< solution.size(); ++i) {
        for (unsigned int j = i + 1; j < solution.size(); ++j) {
            std::pair<int, int> Timeoverlap;
            getTimeOverlap(startTimes[i], startTimes[j], solution[i].states.back().first.time, solution[j].states.back().first.time, Timeoverlap);
            if(Timeoverlap.first != 0){
                double shortestDist = 0;
                int safeDist = radii[i] + radii[j];
                getShortestDist(Timeoverlap, speeds[i], speeds[j], solution[i].states, solution[j].states, startTimes[i], startTimes[j], shortestDist);
                if(shortestDist - safeDist <= 0 ){
                    ++numConflicts;
                }
            }
        }
    }

 /*   int max_t = 0;
    for (const auto& sol : solution) {
      max_t = std::max<int>(max_t, sol.states.size() - 1);//the longest time duration of  paths
    }

    for (int t = 0; t < max_t; ++t) {
      // check drive-drive vertex collisions
      for (size_t i = 0; i < solution.size(); ++i) {
        State state1 = getState(i, solution, t);
        for (size_t j = i + 1; j < solution.size(); ++j) {
          State state2 = getState(j, solution, t);
          if (state1.equalExceptTime(state2)) {
            ++numConflicts;
          }
        }
      }
      // drive-drive edge (swap)
      for (size_t i = 0; i < solution.size(); ++i) {
        State state1a = getState(i, solution, t);
        State state1b = getState(i, solution, t + 1);
        for (size_t j = i + 1; j < solution.size(); ++j) {
          State state2a = getState(j, solution, t);
          State state2b = getState(j, solution, t + 1);
          if (state1a.equalExceptTime(state2b) &&
              state1b.equalExceptTime(state2a)) {
            ++numConflicts;
          }
        }
      }
    }
    */
    return numConflicts;
  } // modified by quanqi

  void SpaTemPruning(const std::vector<PlanResult<State, Action, int>>& solution,
                             const std::vector<int>& radii, const std::vector<float>& speeds, const std::vector<int>& startTimes,
                             std::vector<std::vector<PlanResult<State, Action, int>> >& solutionswithConflicts ){
      for (unsigned int i = 0; i< solution.size(); ++i) {
          for (unsigned int j = i + 1; j < solution.size(); ++j) {
              std::pair<int, int> Timeoverlap;
              getTimeOverlap(startTimes[i], startTimes[j], solution[i].states.back().first.time, solution[j].states.back().first.time, Timeoverlap);
              if(Timeoverlap.first != 0){
                  double shortestDist = 0;
                  int safeDist = radii[i] + radii[j];
                  getShortestDist(Timeoverlap, speeds[i], speeds[j], solution[i].states, solution[j].states, startTimes[i], startTimes[j], shortestDist);
                  if(shortestDist - safeDist <= 0 ){
                      solutionswithConflicts[i].push_back(solution[j]);
                      solutionswithConflicts[j].push_back(solution[i]);
                  } // the data types are different, may not be right
              }
          }

      }
  } // added by quanqi, but not be

  void getTimeOverlap(const int& start1, const int& start2, const int& end1, const int& end2, std::pair<int, int>& overlap){
      if(start1 == start2 && end1 == end2){
          overlap.first = start1; // the variable to be modified must not be const
          overlap.second = end1;
      }
      else if( start1 < start2 && end1 < end2){
          overlap.first = start2;
          overlap.second = end1;
      }
      else if (start1 < start2 && end1 > end2) {
          overlap.first = start2;
          overlap.second = end2;
      }
      else if (start1 > start2 && end1 > end2) {
          overlap.first = start1;
          overlap.second = end2;
      }
      else if (start1 > start2 && end1 < end2) {
          overlap.first = start1;
          overlap.second = end1;
      }
      else if (start1 > end2 || start2 > end1 ) {
          overlap.first = 0;
          overlap.second = 0;
      }
  } // added by quanqi, to ensure that in this timeoverlap both UAVs are moving

  void getShortestDist(const std::pair<int, int>& overlap, const float& speed1, const float& speed2,
                       const std::vector<std::pair<State, int>>& sol1, const std::vector<std::pair<State, int>>& sol2,
                       const int& start1, const int& start2,
                       double& shortestDist){
      int low = overlap.first;
      int up = overlap.second;
      size_t t_long1 = sol1.size();
      size_t t_long2 = sol2.size();
      double sd = 0;
      for (int i = low; i < up +1; ++i) {
          State curState1;
          State curState2;
          // to determine at which step has this uav achieved
          size_t curStep1 =  i - start1;
          size_t curStep2 =  i - start2;
          if (curStep1 < t_long1 && curStep2 < t_long2) { // while these two agents both are moving
          curState1 = sol1[curStep1].first;
          curState2 = sol2[curStep2].first;
          }//else if ( curStep1 > t_long1 || curStep2 > t_long2 ){ // if one of them has reached the goal, there is no need to calculate the distance
          //curState1 = sol1.back().first;
          //curState2 = sol2.back().first;
          //break;
          //}

          double Dist = sqrt(pow(curState1.x - curState2.x, 2) + pow(curState1.y - curState2.y, 2) + pow(curState1.z - curState2.z, 2));
          bool less = true;
          if(Dist - sd > 0 && sd != 0){
              less = false;
          }
          if(Dist - sd < 0){
              less = true;
          }

          if (less){
              sd = Dist;
          }

//          if(Dist - temp < 0){
//              sd = Dist;
//          }
          //temp = Dist;
      }
      shortestDist = 20 * sd;
  } // added by quanqi


  bool isSolution(const State& s) {
      //std::cout << "TEST8" << std::endl;
    return s.x == m_goals[m_agentIdx].x && s.y == m_goals[m_agentIdx].y && s.z == m_goals[m_agentIdx].z &&
           s.time > m_lastGoalConstraint;
  }

  void getNeighbors(const State& s, const float& speed, std::vector<Neighbor<State, Action, int> >& neighbors) {
    // std::cout << "#VC " << constraints.vertexConstraints.size() << std::endl;
    // for(const auto& vc : constraints.vertexConstraints) {
    //   std::cout << "  " << vc.time << "," << vc.x << "," << vc.y <<
    //   std::endl;
    // const float& speed, }
    neighbors.clear();
    if ( ceil(speed) == floor(speed)){ // speed is an interger
        if (int(speed) - 1 == 0){ // speed equal to 1
            {
              State n(s.time + 1, s.x, s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Wait, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y , s.z + 1 );
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Up, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y, s.z -1 );
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Down, 1));
              }
            }
            {
              State n(s.time + 1, s.x - 1 , s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Left, 1));
              }
            }
            {
              State n(s.time + 1, s.x + 1, s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Right, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y + 1, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Accelerate, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y - 1 , s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Deaccelerate, 1));
              }
            }
        } else if (int(speed) - 1 > 0) { // speed bigger than 1
            for ( int v = 1; v < int(speed); v++) {
                {
                  State n(s.time, s.x - v , s.y, s.z);
                  if (stateValid(n) && transitionValid(s, n)) {
                    neighbors.emplace_back(
                        Neighbor<State, Action, int>(n, Action::Left, 1));
                  }
                }
                {
                  State n(s.time, s.x + v, s.y, s.z);
                  if (stateValid(n) && transitionValid(s, n)) {
                    neighbors.emplace_back(
                        Neighbor<State, Action, int>(n, Action::Right, 1));
                  }
                }
                {
                  State n(s.time, s.x, s.y + v, s.z);
                  if (stateValid(n) && transitionValid(s, n)) {
                    neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Accelerate, 1));
                  }
                }
                {
                  State n(s.time, s.x, s.y - v , s.z);
                  if (stateValid(n) && transitionValid(s, n)) {
                    neighbors.emplace_back(
                        Neighbor<State, Action, int>(n, Action::Deaccelerate, 1));
                  }
                }
            }// end else if
            // state after 1 sec
            {
              State n(s.time + 1, s.x, s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Wait, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y , s.z + 1 );
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Up, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y, s.z -1 );
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Down, 1));
              }
            }
            {
              State n(s.time + 1, s.x - int(speed) , s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Left, 1));
              }
            }
            {
              State n(s.time + 1, s.x + int(speed), s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Right, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y + int(speed), s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Accelerate, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y - int(speed) , s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Deaccelerate, 1));
              }
            } // end for
         } // end else if
    }
    else { // speed is not an interger
        {
          State n(s.time + 1, s.x, s.y, s.z);
          if (stateValid(n) && transitionValid(s, n)) {
            neighbors.emplace_back(
                Neighbor<State, Action, int>(n, Action::Wait, 1));
          }
        }
        {
          State n(s.time + 1, s.x, s.y , s.z + 1 );
          if (stateValid(n) && transitionValid(s, n)) {
            neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Up, 1));
          }
        }
        {
          State n(s.time + 1, s.x, s.y, s.z -1 );
          if (stateValid(n) && transitionValid(s, n)) {
            neighbors.emplace_back(
                Neighbor<State, Action, int>(n, Action::Down, 1));
          }
        }
        if (speed - 1 < 0){
            {
              State n(s.time + 1, s.x - int(ceil(speed)) , s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Left, 1));
              }
            }
            {
              State n(s.time + 1, s.x + int(ceil(speed)), s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Right, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y + int(ceil(speed)), s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Accelerate, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y - int(ceil(speed)) , s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Deaccelerate, 1));
              }
            }
        }
        else {
            {
              State n(s.time + 1, s.x - int(floor(speed)) , s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Left, 1));
              }
            }
            {
              State n(s.time + 1, s.x + int(floor(speed)), s.y, s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Right, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y + int(floor(speed)), s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(Neighbor<State, Action, int>(n, Action::Accelerate, 1));
              }
            }
            {
              State n(s.time + 1, s.x, s.y - int(floor(speed)) , s.z);
              if (stateValid(n) && transitionValid(s, n)) {
                neighbors.emplace_back(
                    Neighbor<State, Action, int>(n, Action::Deaccelerate, 1));
              }
            }
        }

    }
  }

  bool getConflictInterval(const std::pair<int, int>& Timeoverlap, const float& speed1, const float& speed2,
                           const std::vector<std::pair<State, int>>& sol1, const std::vector<std::pair<State, int>>& sol2,
                           const int& r1, const int& r2, std::pair<int, int> intervalConflict){

      int lowTime = Timeoverlap.first;
      int upTime = Timeoverlap.second;
      int interval_low = 0;
      int interval_up = 0;
      size_t t_long3 = sol1.size();
      size_t t_long4 = sol2.size();
      // one timestep is equal to a state of a solution for agent i sequentially
      for (int i = lowTime; i < upTime+1; ++i) { // in between the common time interval
          State curState3;
          State curState4;
          size_t curStep3 = i - sol1[0].first.time; // get current timestep of agent i, how many states has agent i driven through till current timestep
          size_t curStep4 = i - sol2[0].first.time; // get current timestep of agent j, one timestep is corresponded to one state
          if ( curStep3 < t_long3 && curStep4 < t_long4){ // while the two agents are both moving
              curState3 = sol1[curStep3].first; // get current state of agent i
              curState4 = sol2[curStep4].first; // get current state of agent j
              // calculate the distance between the two agents

              double curDist = 20 * sqrt(pow(curState3.x - curState4.x, 2) + pow(curState3.y - curState4.y, 2) + pow(curState3.z - curState4.z, 2));
              int collisionBound = r1 + r2; // get the safety bound
              bool firstlow = false;
              if(curDist - collisionBound < 0 && firstlow == false){ // get the first timestep, when agent comes into conflict
                  interval_low = i - 1;
                  firstlow = true;
              }
              if(curDist - collisionBound > 0 && firstlow == true){
                  interval_up = i;
                  intervalConflict.first = interval_low;
                  intervalConflict.second = interval_up;
                  return true;
              }
          }

      }
      if(intervalConflict.first == 0 && intervalConflict.second == 0 ) return false;

  } // added by quanqi, the pre condition for call on this function is the appearance of a conflict

  bool getFirstConflict(
      const std::vector<PlanResult<State, Action, int> >& solution,
          const std::vector<int>& radii, const std::vector<float>& speeds, const std::vector<int>& startTimes,
      Conflict1& result) {

      for (unsigned int i = 0; i< solution.size(); ++i) {
          for (unsigned int j = i + 1; j < solution.size(); ++j) {
              std::pair<int, int> Timeoverlap;
              getTimeOverlap(startTimes[i], startTimes[j], solution[i].states.back().first.time, solution[j].states.back().first.time, Timeoverlap);
              if(Timeoverlap.first != 0){
                  double shortestDist = 0;
                  int safeDist = radii[i] + radii[j];
                  getShortestDist(Timeoverlap, speeds[i], speeds[j], solution[i].states, solution[j].states, startTimes[i], startTimes[j], shortestDist);
                  if(shortestDist - safeDist <= 0 ){ // a conflict appears
                      std::pair<int, int> intervalConflict; // declare a variable pair as conflict interval
                      // get the conflict interval Ic
                      if(getConflictInterval(Timeoverlap, speeds[i], speeds[j], solution[i].states, solution[j].states, radii[i], radii[j], intervalConflict)){
                          result.conflictInterval.first = intervalConflict.first;
                          result.conflictInterval.second = intervalConflict.second;
                          result.agent1 = i;
                          result.agent2 = j;
                          result.radius1 = radii[i];
                          result.radius2 = radii[j];
                          result.speed1 = speeds[i];
                          result.speed2 = speeds[j];
                          result.x1 = solution[i].states[intervalConflict.first].first.x; // agent i at tc
                          result.y1 = solution[i].states[intervalConflict.first].first.y;
                          result.z1 = solution[i].states[intervalConflict.first].first.z;
                          result.x2 = solution[j].states[intervalConflict.first].first.x; // agent i at tc
                          result.y2 = solution[j].states[intervalConflict.first].first.y;
                          result.z2 = solution[j].states[intervalConflict.first].first.z;
                          return true;
                      }

                  } // the data types are different, may not be right
              }
          }

      }
/*    int max_t = 0;
    for (const auto& sol : solution) {
      max_t = std::max<int>(max_t, sol.states.size() - 1);
    }
*/
 /*   for (int t = 0; t < max_t; ++t) {
      // check drive-drive vertex collisions
      for (size_t i = 0; i < solution.size(); ++i) {
        State state1 = getState(i, solution, t);
        for (size_t j = i + 1; j < solution.size(); ++j) {
          State state2 = getState(j, solution, t);
          if (state1.equalExceptTime(state2)) {
            result.time = t;
            result.agent1 = i;
            result.agent2 = j;
            result.type = Conflict::Vertex;
            result.x1 = state1.x;
            result.y1 = state1.y;
            result.z1 = state1.z;
            // std::cout << "VC " << t << "," << state1.x << "," << state1.y <<
            // std::endl;
            return true;
          }
        }
      }
      // drive-drive edge (swap)
      for (size_t i = 0; i < solution.size(); ++i) {
        State state1a = getState(i, solution, t);
        State state1b = getState(i, solution, t + 1);
        for (size_t j = i + 1; j < solution.size(); ++j) {
          State state2a = getState(j, solution, t);
          State state2b = getState(j, solution, t + 1);
          if (state1a.equalExceptTime(state2b) &&
              state1b.equalExceptTime(state2a)) {
            result.time = t;
            result.agent1 = i;
            result.agent2 = j;
            result.type = Conflict::Edge;
            result.x1 = state1a.x;
            result.y1 = state1a.y;
            result.z1 = state1a.z;
            result.x2 = state1b.x;
            result.y2 = state1b.y;
            result.z2 = state1b.z;
            return true;
          }
        }
      }
    }
*/
    return false;
  } // hier must be modified to compute conflict interval, modified by quanqi

  void createConstraintsFromConflict(const Conflict1& conflict, const std::vector<std::pair<State, int>>& sol1, const std::vector<std::pair<State, int>>& sol2, const int& start1, const int& start2, std::map<size_t, Constraints1>& constraints) {
   // if (conflict.type == Conflict::Vertex) {
      Constraints1 c1;
      for (int i = conflict.conflictInterval.first; i < conflict.conflictInterval.second +1; i++) {
          c1.allConstraints.emplace(
             Constraint1( sol1[i - start1].first.x, sol1[i - start1].first.y, sol1[i - start1].first.z, sol1[i - start1].first.time));
      }
//          c1.allConstraints.emplace(
//          Constraint1( conflict.x1, conflict.y1, conflict.z1, conflict.radius1, conflict.speed1,
//                      conflict.x2, conflict.y2, conflict.z2, conflict.radius2, conflict.speed2, conflict.conflictInterval));
      Constraints1 c2;
      for (int i = conflict.conflictInterval.first; i < conflict.conflictInterval.second +1; i++) {
          c2.allConstraints.emplace(
             Constraint1( sol2[i - start2].first.x, sol2[i - start2].first.y, sol2[i - start2].first.z, sol2[i - start2].first.time));
      }
      constraints[conflict.agent1] = c1; // constraints[i]
      constraints[conflict.agent2] = c2; // constraints[j]
  /*  } else if (conflict.type == Conflict::Edge) {
      Constraints c1;
      c1.edgeConstraints.emplace(EdgeConstraint(
          conflict.time, conflict.x1, conflict.y1, conflict.z1, conflict.x2, conflict.y2, conflict.z2));
      constraints[conflict.agent1] = c1;
      Constraints c2;
      c2.edgeConstraints.emplace(EdgeConstraint(
          conflict.time, conflict.x2, conflict.y2, conflict.z2, conflict.x1, conflict.y1, conflict.z1));
      constraints[conflict.agent2] = c2;
    }
    */
  } // modify the edge into geometrical, cancel the vertex, by quanqi



  void onExpandHighLevelNode(int /*cost*/) { m_highLevelExpanded++; }

  void onExpandLowLevelNode(const State& /*s*/, int /*fScore*/,
                            int /*gScore*/) {
    m_lowLevelExpanded++;
  }

  int highLevelExpanded() { return m_highLevelExpanded; }

  int lowLevelExpanded() const { return m_lowLevelExpanded; }

 private:
  State getState(size_t agentIdx,
                 const std::vector<PlanResult<State, Action, int> >& solution,
                 size_t t) {
    assert(agentIdx < solution.size());
    if (t < solution[agentIdx].states.size()) {
      return solution[agentIdx].states[t].first;
    }
    assert(!solution[agentIdx].states.empty());
    return solution[agentIdx].states.back().first;
  }

  bool stateValid(const State& s) {
    assert(m_constraints);
    const auto& con = m_constraints->allConstraints;
    return s.x >= 0 && s.x < m_dimx && s.y >= 0 && s.y < m_dimy && s.z >=0 && s.z < m_dimz &&
           m_obstacles.find(Location(s.x, s.y, s.z)) == m_obstacles.end() &&
           con.find(Constraint1(s.time, s.x, s.y, s.z)) == con.end() &&
            con.find(Constraint1(s.time, s.time, s.x, s.y, s.z)) == con.end();
  } // modified ny quanqi, must be considered later

  bool transitionValid(const State& s1, const State& s2) {
    assert(m_constraints);
    const auto& con = m_constraints->allConstraints;
    return con.find(Constraint1(s2.time, s2.x, s2.y, s2.z, s2.time)) ==
           con.end();
  } // must be considered later

 private:
  int m_dimx;
  int m_dimy;
  int m_dimz;
  std::unordered_set<Location> m_obstacles;
  std::vector<Location> m_goals;
  size_t m_agentIdx;
  const Constraints1* m_constraints;
  int m_lastGoalConstraint;
  int m_highLevelExpanded;
  int m_lowLevelExpanded;
};

int main(int argc, char* argv[]) {
  namespace po = boost::program_options;
  // Declare the supported options.
  po::options_description desc("Allowed options");
  std::string inputFile;
  std::string outputFile;
  float w = 1.0;
  desc.add_options()("help", "produce help message")(
      "input,i", po::value<std::string>(&inputFile)->required(),
      "input file (YAML)")("output,o",
                           po::value<std::string>(&outputFile)->required(),
                           "output file (YAML)")(
      "suboptimality,w", po::value<float>(&w)->default_value(1.0),
      "suboptimality bound");

  try {
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help") != 0u) {
      std::cout << desc << "\n";
      return 0;
    }
  } catch (po::error& e) {
    std::cerr << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return 1;
  }



  YAML::Node config = YAML::LoadFile(inputFile);
  //YAML::Node config = YAML::LoadFile("/home/quanqi/qt_trial/build-mapf_trial-Desktop-Default/input_5G_env_surv.yaml");



  std::unordered_set<Location> obstacles; // static obstacles, those in which the UAV can not drive
  std::vector<Location> goals;
  std::vector<State> startStates;
  std::vector<int> radii;
  std::vector<float> speeds;
  std::vector<int> startTimes; // the data type of time can be int or float, must be determined



  const auto& dim = config["map"]["dimensions"];
  int dimx = dim[0].as<int>();
  int dimy = dim[1].as<int>();
  int dimz = dim[2].as<int>();



  for (const auto& node : config["map"]["obstacles"]) {
    obstacles.insert(Location(node[0].as<int>(), node[1].as<int>(), node[2].as<int>()));
  }



  for (const auto& node : config["agents"]) {
    const auto& start = node["start"];


    const auto& goal = node["goal"];

    const auto& radius = node["radius"];

    const auto& speed = node["speed"];

    const auto& startTime = node["startTimeStep"];

    startStates.emplace_back(State(startTime.as<int>(), start[0].as<int>(), start[1].as<int>(), start[2].as<int>())); // where to set the truly start time, modified ny quanqi
    // std::cout << "s: " << startStates.back() << std::endl;startTime[0].as<int>()

    goals.emplace_back(Location(goal[0].as<int>(), goal[1].as<int>(), goal[2].as<int>()));

    radii.emplace_back(radius.as<int>()); // if the data type is not int, please use .as to change he type

    speeds.emplace_back(speed.as<float>());

    startTimes.emplace_back(startTime.as<int>());
  }// here add the other characteristics of agents, some new structs must also be constructed



  Environment mapf(dimx, dimy, dimz, obstacles, goals);
  ECBS<State, Action, int, Conflict1, Constraints1, Environment> cbs(mapf, w);
  std::vector<PlanResult<State, Action, int> > solution;// give more elements to solution

  Timer timer;
  bool success = cbs.search(startStates, radii, speeds, startTimes, solution); // modified by quanqi
  timer.stop();

  if (success) {
    std::cout << "Planning successful! " << std::endl;
    int cost = 0;
    int makespan = 0;
    for (const auto& s : solution) {
      cost += s.cost;
      makespan = std::max<int>(makespan, s.cost);
    }

    std::ofstream out(outputFile);
    //std::ofstream out("output_test_speed.yaml");
    out << "statistics:" << std::endl;
    out << "  cost: " << cost << std::endl;
    out << "  makespan: " << makespan << std::endl;
    out << "  runtime: " << timer.elapsedSeconds() << std::endl;
    out << "  highLevelExpanded: " << mapf.highLevelExpanded() << std::endl;
    out << "  lowLevelExpanded: " << mapf.lowLevelExpanded() << std::endl;
    out << "schedule:" << std::endl;
    for (size_t a = 0; a < solution.size(); ++a) {
      // std::cout << "Solution for: " << a << std::endl;
      // for (size_t i = 0; i < solution[a].actions.size(); ++i) {
      //   std::cout << solution[a].states[i].second << ": " <<
      //   solution[a].states[i].first << "->" << solution[a].actions[i].first
      //   << "(cost: " << solution[a].actions[i].second << ")" << std::endl;
      // }
      // std::cout << solution[a].states.back().second << ": " <<
      // solution[a].states.back().first << std::endl;

      out << "  agent" << a << ":" << std::endl;
      for (const auto& state : solution[a].states) {
        out << "    - x: " << state.first.x << std::endl
            << "      y: " << state.first.y << std::endl
            << "      z: " << state.first.z << std::endl
            << "      t: " << state.first.time << std::endl;
            //<< "      t: " << state.second << std::endl;
      }
    }
  } else {
    std::cout << "Planning NOT successful!" << std::endl;
  }

  return 0;
}
